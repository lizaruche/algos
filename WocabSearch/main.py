import json


class Job:
    jobs = {}
    current_max_priority = 0
    name: str
    priority: int | None
    vocab_priority: str | None
    done: bool
    parents: list[object] | None
    children: list[object] | None

    def __init__(self, name: str, parents: list[object], children: list[object], tmp_job=False):
        self.name = name
        self.parents = parents
        self.children = children
        self.done = tmp_job
        self.vocab_priority = None
        self.priority = None
        if not tmp_job:
            Job.jobs.update({self.name: self})

    def __str__(self):
        return self.name + " " + str(self.priority) + " " + str(len(self.parents))

    def append_parent(self, parent: object):
        self.parents.append(parent)

    def append_child(self, child: object):
        self.children.append(child)

    def num_of_parents(self) -> int | None:
        return len(self.parents) if len(self.parents) > 0 else None

    def num_of_children(self) -> int | None:
        return len(self.children) if len(self.children) > 0 else None

    def set_vocab_priority(self):
        tmp_vocab_list = []
        no_root_priority = False
        for child in self.children:
            if child.priority is not None:
                tmp_vocab_list.append(child.priority)
            else:
                no_root_priority = True
                break

        if not no_root_priority:
            tmp_vocab_list.sort(reverse=True)
            self.vocab_priority = ''.join(map(str, tmp_vocab_list))

    def can_be_done(self, this_iter_job: str) -> bool:
        result = True
        for parent in self.parents:
            if not parent.done or parent.name == this_iter_job:
                result = False
                break

        return result

    def do(self) -> str:
        self.done = True
        return self.name

    def set_new_priority(self):
        Job.current_max_priority += 1
        self.priority = Job.current_max_priority


def print_jobs(jobs: list[Job]):
    for job in jobs:
        print(job)


def load_from_json(name: str, test_name: str):
    with open(name, mode='r') as data:
        data = json.load(data)
        data = data[test_name]

    for job in data.keys():
        Job(name=job, parents=[], children=[])

    for job_name in data.keys():
        for parent_name in data[job_name]:
            Job.jobs[job_name].append_parent(Job.jobs[parent_name])
            Job.jobs[parent_name].append_child(Job.jobs[job_name])


def get_roots() -> list[Job]:
    result = []
    for job in Job.jobs.values():
        if job.num_of_children() is None:
            result.append(job)

    return result


def set_priorities(roots: list[Job]):
    # key - vocab tmp for job, value - job
    parents = []
    tmp_roots = []
    for root in roots:
        root.set_vocab_priority()
        if root.vocab_priority is None:
            if root not in parents:
                parents.append(root)
        else:
            tmp_roots.append(root)

    roots = tmp_roots
    roots.sort(key=lambda x: x.vocab_priority)
    for root in roots:
        root.set_new_priority()

        for parent in root.parents:
            if parent not in parents:
                parents.append(parent)

    if parents:
        set_priorities(parents)


def all_jobs_done() -> bool:
    for priority in range(1, len(Job.jobs) + 1):
        if not search_by_priority(priority).done:
            return False

    return True


def search_by_priority(priority: int) -> Job | None:
    for job in Job.jobs.values():
        if job.priority == priority:
            return job

    return None


def div_between_workers() -> list[str]:
    workers = ["", ""]
    priorities = [*range(len(Job.jobs), 0, -1)]

    while not all_jobs_done():
        cur_priority = priorities[0]
        this_iter_job = ""
        if (not search_by_priority(cur_priority).done) and search_by_priority(cur_priority).can_be_done(this_iter_job):
            this_iter_job = search_by_priority(cur_priority).do()
            workers[0] += this_iter_job
            priorities.pop(0)
            for priority in priorities:
                if not search_by_priority(priority).done and search_by_priority(priority).can_be_done(this_iter_job):
                    workers[1] += search_by_priority(priority).do()
                    priorities.remove(priority)
                    break
                elif priority == priorities[len(priorities)-1]:
                    workers[1] += "-"

    return workers


def print_schedule(workers: list[str]):
    first_worker = "1 worker jobs: "
    second_worker = "2 worker jobs: "
    for i in range(0, len(workers[0])):
        first_worker += workers[0][i] + " "
        second_worker += workers[1][i] + " "

    print(first_worker)
    print(second_worker)


if __name__ == "__main__":
    load_from_json(name="test_data.json", test_name="test1")
    roots = get_roots()
    for root in roots:
        tmp_job = Job(name="test" + root.name, parents=[], children=[], tmp_job=True)
        tmp_job.priority = 0
        root.append_child(tmp_job)
    set_priorities(roots=roots)
    print_schedule(div_between_workers())
